import wx from 'weixin-js-sdk'
// let wx_conf = {}
// axios.get('https://demo2.gypserver.com/weixinShareFile', {
//     params: {
//         url: window.location.href
//     }
//   })
//   .then(function (response) {
//     let str = response.data
//     let data = str.substring(str.indexOf('{') + 1 , str.length  - 1)
//     let arr = data.split(',')
//     arr.forEach(element => {
//         let newe = element.replace(/\"/g, "");//替换掉所有双引号
//         let ele = newe.split(":");
//         wx_conf[ele[0]] = ele[1]
//     });

//     wx.config({
//         debug: false,
//         appId: wx_conf.appId,
//         timestamp: wx_conf.timestamp,
//         nonceStr: wx_conf.nonceStr,
//         signature: wx_conf.signature,
//         jsApiList: [
//             'checkJsApi',
//             'onMenuShareTimeline',
//             'onMenuShareAppMessage'
//         ]
//     });
//   })
//   .catch(function (error) {
//     console.log(error);
//   });
console.log(wx_conf)
wx.config({
    debug: false,
    appId: wx_conf.appId,
    timestamp: wx_conf.timestamp,
    nonceStr: wx_conf.nonceStr,
    signature: wx_conf.signature,
    jsApiList: [
        'checkJsApi',
        'onMenuShareTimeline',
        'onMenuShareAppMessage'
    ]
});
//分享标题
var wxtitle = '星缘预测';
//分享内容
var wxdesc = '解读你的另一面';
//分享网址
var baseurl = '';
var shareurl = 'https://calpis.gypserver.com/#/';
//分享图片
var imgUrl = 'https://calpis.gypserver.com/static/img/share.png';


wx.ready(function() {
    var shareData = {
        title: wxtitle,
        desc: wxdesc,
        link: shareurl,
        imgUrl: imgUrl,
        success: function() {},
        error: function() {}

    };
    var shareDataline = {
        title: wxtitle,
        desc: wxdesc,
        link: shareurl,
        imgUrl: imgUrl,
        success: function() {},
        error: function() {}
    };
    wx.onMenuShareAppMessage(shareData);
    wx.onMenuShareTimeline(shareDataline);

});